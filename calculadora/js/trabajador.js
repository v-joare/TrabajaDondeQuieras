$(function () {
    var $sections = $('.form-section');

    function navigateTo(index) {
        // Mark the current section with the class 'current'
        $sections
            .removeClass('current')
            .eq(index)
            .addClass('current');
        // Show only the navigation buttons that make sense for the current section:
        $('.form-navigation .previous').toggle(index > 0);
        var atTheEnd = index >= $sections.length - 1;
        $('.form-navigation .next').toggle(!atTheEnd);
        $('.form-navigation [type=submit]').toggle(atTheEnd);
    }

    function curIndex() {
        // Return the current index by looking at which section has the class 'current'
        return $sections.index($sections.filter('.current'));
    }

    // Previous button is easy, just go back
    $('.form-navigation .previous').click(function() {
        navigateTo(curIndex() - 1);
        $('.avance span').html(curIndex() + 1);
    });

    // Next button goes forward iff current block validates
    $('.form-navigation .next').click(function() {
        if ($('.formulario').parsley().validate({group: 'block-' + curIndex()})) {

            var delay = 1000;
            if(curIndex()==2) {
                $('.form-section').hide();
                $('.form-navigation').hide();
                $('.loading').show();
                processCalc();

                setTimeout(function() {
                    $('.loading').hide();
                    navigateTo(curIndex() + 1);
                    var step = curIndex() + 1;
                    $('.avance span').html(step);
                    $('.barra-fondo div').removeClass().addClass("barra-completado-" + step);
                    $('.form-section.current').show();
                }, delay);
            } else {
                navigateTo(curIndex() + 1);
                var step = curIndex() + 1;
                $('.avance span').html(step);
                $('.barra-fondo div').removeClass().addClass("barra-completado-" + step);
            }

        }

    });

    // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
    $sections.each(function(index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });

    navigateTo(0); // Start at the beginning


    function processCalc () {
        var nombre 				= $("#nombre").val();
        var salario 			= parseInt($("#salario").val());
        var tiempo_ida_min 		= parseInt($("#tiempo_ida_min").val());
        var costo_ida 			= parseInt($("#costo_ida").val());
        var tiempo_vuelta_min 	= parseInt($("#tiempo_vuelta_min").val());
        var costo_vuelta 		= parseInt($("#costo_vuelta").val());

        var dias_laborales_anuales = 250;
        var viaticos_diarios = costo_ida + costo_vuelta;
        var viaticos_anuales = viaticos_diarios * dias_laborales_anuales;
        var porcentaje_anual_invertido_en_viaticos = (viaticos_anuales * 100) / salario;
        porcentaje_anual_invertido_en_viaticos = Math.round(porcentaje_anual_invertido_en_viaticos);

        var minutos_totales_diarios = tiempo_ida_min + tiempo_vuelta_min;
        var minutos_totales_anuales = minutos_totales_diarios * dias_laborales_anuales;

        var dias_totales_anuales = minutos_totales_anuales / 60 / 24;
        dias_totales_anuales = Math.round(dias_totales_anuales);

        var porcentaje_grafico_tiempo = (dias_totales_anuales * 100) / dias_laborales_anuales;
        porcentaje_grafico_tiempo = Math.round(porcentaje_grafico_tiempo);

        var porcentaje_grafico_sueldo = (porcentaje_anual_invertido_en_viaticos > 100) ? 100 : porcentaje_anual_invertido_en_viaticos;

        $(".resultados-cont .info span").text(nombre);
        $(".resultados-cont .grafico-sueldo .grafico p").html(porcentaje_anual_invertido_en_viaticos + "<span>%</span><i>Invertido</i>");
        $(".resultados-cont .grafico-tiempo .grafico p").html(dias_totales_anuales + "<span>días</span><i>Al año</i>");

        $(".resultados-cont .grafico-tiempo #porcen-tiempo").removeClass();
        $(".resultados-cont .grafico-tiempo #porcen-tiempo").addClass("c100 center big p" + porcentaje_grafico_tiempo);

        $(".resultados-cont .grafico-sueldo #porcen-sueldo").removeClass();
        $(".resultados-cont .grafico-sueldo #porcen-sueldo").addClass("c100 center big p" + porcentaje_grafico_sueldo);
    }


    $('.volver-calcular').click(function() {
        location.reload();
    });


});