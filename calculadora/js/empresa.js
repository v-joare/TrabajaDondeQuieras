$(function () {
    var $sections = $('.form-section');

    function navigateTo(index) {
        // Mark the current section with the class 'current'
        $sections
            .removeClass('current')
            .eq(index)
            .addClass('current');
        // Show only the navigation buttons that make sense for the current section:
        $('.form-navigation .previous').toggle(index > 0);
        var atTheEnd = index >= $sections.length - 1;
        $('.form-navigation .next').toggle(!atTheEnd);
        $('.form-navigation [type=submit]').toggle(atTheEnd);
    }

    function curIndex() {
        // Return the current index by looking at which section has the class 'current'
        return $sections.index($sections.filter('.current'));
    }

    // Previous button is easy, just go back
    $('.form-navigation .previous').click(function() {
        navigateTo(curIndex() - 1);
        $('.avance span').html(curIndex() + 1);
    });

    // Next button goes forward iff current block validates
    $('.form-navigation .next').click(function() {
        if ($('.formulario').parsley().validate({group: 'block-' + curIndex()})) {

            var delay = 1000;
            if(curIndex()==2) {
                $('.form-section').hide();
                $('.form-navigation').hide();
                $('.loading').show();
                processCalc();

                setTimeout(function() {
                    $('.loading').hide();
                    navigateTo(curIndex() + 1);
                    var step = curIndex() + 1;
                    $('.avance span').html(step);
                    $('.barra-fondo div').removeClass().addClass("barra-completado-" + step);
                    $('.form-section.current').show();
                }, delay);
            } else {
                navigateTo(curIndex() + 1);
                var step = curIndex() + 1;
                $('.avance span').html(step);
                $('.barra-fondo div').removeClass().addClass("barra-completado-" + step);
            }

        }

    });

    // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
    $sections.each(function(index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });

    navigateTo(0); // Start at the beginning


    function processCalc () {
        var teletrabajadores 	= parseInt($("#teletrabajadores").val());
        var dias 		        = parseInt($("#dias").val());
        var mobiliario 			= parseInt($("#mobiliario").val());
        var arrendamiento 	    = parseInt($("#arrendamiento").val());

        var a = teletrabajadores * mobiliario;
        var b = arrendamiento * 2 * teletrabajadores * 12;
        var tot = a + b;
        tot = Math.round(tot);
        tot = tot.toLocaleString();

        $(".resultados-cont .grafico-empresa p").html("$" + tot);
        $(".resultados-cont p.info span").html(teletrabajadores + " Teletrabajadores");
    }


    $('.volver-calcular').click(function() {
        location.reload();
    });


});