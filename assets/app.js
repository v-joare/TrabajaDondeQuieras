// PAGE-SCROLL
$('a.page-scroll').bind('click', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top-10
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
});
// FIN PAGE-SCROLL

//DESPLEGAR MENU
$ (document).ready(function() {
    $('.desplegar_menu').click(function() {
        if ( $("#menu_escondido").is( ":hidden" ) ) {
            $("#menu_escondido").show();
        }else{
            $("#menu_escondido").hide();
        }
    }); 
});
//FIN DESPLEGAR MENU
    
//CAMBIO DE COLOR FLECHA A:HOVER
